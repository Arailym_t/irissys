<?php

namespace App\Http\Controllers;

use App\Http\Requests\PassportRequest;
use App\Models\Passport;
use Illuminate\Http\Request;

class PassportController extends Controller
{
//    public $passport_id;

    public function checkPassport(PassportRequest $passportRequest)
    {
        try {
            $mrz = $passportRequest->input('mrz');
            $asd = Passport::whereMrz($mrz)->get()->toArray();
//            $this->passport_id  = $asd['passport_id'];
            return $asd;
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }
}
